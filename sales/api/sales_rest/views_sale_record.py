from django.http import JsonResponse
from django.views.decorators.http import require_http_methods
import json
from .models import SaleRecord, AutomobileVO, SalesPerson, Customer
from .detail_encoders import  SaleRecordDetailEncoder


@require_http_methods(["GET","POST"])
def api_all_sale_records(request):
    if request.method == "GET":
        sale_records = SaleRecord.objects.all()
        return JsonResponse(
            {"sale_records": sale_records},
            encoder=SaleRecordDetailEncoder
        )
    else:
        content = json.loads(request.body)



        check_auto_id = content["automobile"]
        check_automobile = AutomobileVO.objects.get(id=check_auto_id)
        if (check_automobile["status"] != "In Stock"):
            return JsonResponse(
                {"message": "Automobile is already sold"}
            )

        try:
            customer_id = content["customer"]
            customer = Customer.objects.get(id=customer_id)
            content["customer"]=customer
        except:
            return JsonResponse(
                {"message": "Invalid customer id"}
            )

        try:
            auto_id = content["automobile"]
            automobile = AutomobileVO.objects.get(id=auto_id)
            automobile.sold()
            content["automobile"]=automobile

        except:
            return JsonResponse(
                {"message": "Invalid automobile id"}
            )

        try:
            person_id = content["sales_person"]
            sales_person = SalesPerson.objects.get(id=person_id)
            content["sales_person"]=sales_person
        except:
            return JsonResponse(
                {"message": "Invalid sales person id"}
            )



        SaleRecord.objects.create(**content)
        sale_record = SaleRecord.objects.all()[len(SaleRecord.objects.all())-1]
        return JsonResponse(
            {"sale_record": sale_record},
            encoder=SaleRecordDetailEncoder,
            safe=False
        )







@require_http_methods(["DELETE", "GET", "PUT"])
def api_sale_record(request, id):
    if request.method == "GET":
        try:
            sale_record = SaleRecord.objects.get(id=id)
            return JsonResponse(
                sale_record,
                encoder=SaleRecordDetailEncoder,
                safe=False
            )
        except SaleRecord.DoesNotExist:
            return JsonResponse({"message": "Sales Record not exist"})
    elif request.method == "PUT":
        try:
            content = json.loads(request.body)
            sale_record = SaleRecord.objects.get(id=id)


            if "sale_price" in content:
                setattr(sale_record, "sale_price", (content["sale_price"]))

            if "sales_person" in content:
                person_id = content["sales_person"]
                sales_person = SalesPerson.objects.get(id=person_id)
                setattr(sale_record, "sales_person", sales_person)

            if "automobile" in content:
                auto_id = content["automobile"]
                auto = AutomobileVO.objects.get(id=auto_id)
                setattr(sale_record, "automobile", auto)

            if "customer" in content:
                customer_id = content["customer"]
                customer = Customer.objects.get(id=customer_id)
                setattr(sale_record, "customer", customer)

            sale_record.save()
            show_sale_record = SaleRecord.objects.get(id=id)
            return JsonResponse(
                    show_sale_record,
                        encoder=SaleRecordDetailEncoder,
                        safe=False)


        except SaleRecord.DoesNotExist:
            response = JsonResponse({"message": "Sales Record does not exist"})
            response.status_code = 400
            return response
    else:
        count, _ = SaleRecord.objects.filter(id=id).delete()
        return JsonResponse({"deleted": count>0})





@require_http_methods(["GET","POST"])
def api_sale_records_per_sales_person(request, id):
    if request.method == "GET":
        sale_records = SaleRecord.objects.filter(sales_person__id=id)
        return JsonResponse(
            {"sale_records": sale_records},
            encoder=SaleRecordDetailEncoder
        )
