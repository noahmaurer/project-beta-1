from django.http import JsonResponse
from django.views.decorators.http import require_http_methods
import json
from .models import AutomobileVO
from .detail_encoders import AutomobileVODetailEncoder

@require_http_methods(["GET"])
def api_all_automobile_vo(request):
    if request.method == "GET":
        autos = AutomobileVO.objects.all()
        return JsonResponse(
            {"autos":autos},
            encoder=AutomobileVODetailEncoder
        )
