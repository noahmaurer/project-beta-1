from django.urls import path
from .views_sales_person import api_all_sales_persons, api_sales_person
from .views_customer import api_all_customers, api_customer
from .views_sale_record import api_all_sale_records, api_sale_record, api_sale_records_per_sales_person
from .views_automobile_vo import api_all_automobile_vo


urlpatterns = [
    path("sales_persons/", api_all_sales_persons, name="api_all_sales_persons"),
    path("sales_persons/<int:id>/", api_sales_person, name="api_sales_person"),
    path("customers/",api_all_customers, name="api_all_customers" ),
    path("customers/<int:id>/",api_customer, name="api_customer" ),
    path("sale_records/", api_all_sale_records, name="api_all_sale_records"),
    path("sale_records/<int:id>/", api_sale_record, name="api_sale_record"),
    path("automobiles/", api_all_automobile_vo, name="api_all_automobile_vo"),
    path("sales_person/sale_records/<int:id>/", api_sale_records_per_sales_person, name="api_sale_records_per_sales_person")
]
