from django.http import JsonResponse
from django.views.decorators.http import require_http_methods
import json
from .models import SalesPerson
from .detail_encoders import SalesPersonEncoder, SalesPersonDetailEncoder

@require_http_methods(["GET","POST"])
def api_all_sales_persons(request):
    if request.method == "GET":
        sales_persons = SalesPerson.objects.all()
        return JsonResponse(
            {"sales_persons": sales_persons},
            encoder=SalesPersonEncoder
        )
    else:
        content = json.loads(request.body)
        sales_person = SalesPerson.objects.create(**content)
        return JsonResponse(
            sales_person,
            encoder = SalesPersonEncoder,
            safe=False
        )

@require_http_methods(["DELETE", "GET", "PUT"])
def api_sales_person(request, id):
    if request.method == "GET":
        try:
            sales_person = SalesPerson.objects.get(id=id)
            return JsonResponse(
                sales_person,
                encoder=SalesPersonEncoder,
                safe=False
            )
        except SalesPerson.DoesNotExist:
            return JsonResponse({"message": "Sales Person not exist"})
    elif request.method == "PUT":
        try:
            content = json.loads(request.body)
            sales_person = SalesPerson.objects.get(id=id)

            props = ["employee_number", "name"]
            for prop in props:
                if prop in content:
                    setattr(sales_person, prop, content[prop])
            sales_person.save()
            return JsonResponse(
                    sales_person,
                    encoder = SalesPersonDetailEncoder,
                    safe=False)

        except SalesPerson.DoesNotExist:
            response = JsonResponse({"message": "Sales Person does not exist"})
            response.status_code = 400
            return response
    else:
        count, _ = SalesPerson.objects.filter(id = id).delete()
        return JsonResponse({"deleted": count>0})
