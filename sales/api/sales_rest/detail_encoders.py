from .models import AutomobileVO, SalesPerson, Customer, SaleRecord
from common.json import ModelEncoder
import json
from decimal import Decimal

class AutomobileVODetailEncoder(ModelEncoder):
    model = AutomobileVO
    properties = [
        "id",
        "color",
        "year",
        "vin",
        "import_href",
        "status",
    ]

class SalesPersonEncoder(ModelEncoder):
    model = SalesPerson
    properties = [
        "employee_number",
        "name",
        "id"
    ]

class SalesPersonDetailEncoder(ModelEncoder):
    model = SalesPerson
    properties = [
        "employee_number",
        "name",
        "id",
    ]

class CustomerEncoder(ModelEncoder):
    model = Customer
    properties = [
        "address",
        "name",
        "phone_number",
        "id",
    ]

class CustomerDetailEncoder(ModelEncoder):
    model = Customer
    properties = [
        "address",
        "name",
        "phone_number",
        "id"
    ]

class DecimalEncoder(json.JSONEncoder):
    def default(self, obj):
        if isinstance(obj, Decimal):
            # return f"${str(obj)}"
            return str(obj)
        return json.JSONEncoder.default(self, obj)

class SaleRecordDetailEncoder(ModelEncoder):
    model = SaleRecord
    properties = [
        "id",
        "sale_price",
        "automobile",
        "sales_person",
        "customer",

    ]
    encoders={"automobile": AutomobileVODetailEncoder(),
              "sales_person": SalesPersonDetailEncoder(),
              "customer": CustomerDetailEncoder(),
              "sale_price": DecimalEncoder()  }
