from django.contrib import admin
from .models import Customer, AutomobileVO, SalesPerson, SaleRecord

admin.site.register(Customer)
admin.site.register(AutomobileVO)
admin.site.register(SalesPerson)
admin.site.register(SaleRecord)
