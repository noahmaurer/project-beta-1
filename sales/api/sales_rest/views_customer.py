from django.http import JsonResponse
from django.views.decorators.http import require_http_methods
import json
from .models import Customer
from .detail_encoders import  CustomerEncoder

@require_http_methods(["GET","POST"])
def api_all_customers(request):
    if request.method== "GET":
        customers = Customer.objects.all()
        return JsonResponse(
            {"customers": customers},
            encoder=CustomerEncoder
        )
    else:
        content = json.loads(request.body)
        customer = Customer.objects.create(**content)
        return JsonResponse(
            customer,
            encoder=CustomerEncoder,
            safe=False
        )
@require_http_methods(["DELETE", "GET","PUT"])
def api_customer(request, id):
    if request.method == "GET":
        try:
            customer = Customer.objects.get(id=id)
            return JsonResponse(
                customer,
                encoder=CustomerEncoder,
                safe=False
            )
        except Customer.DoesNotExist:
            response = JsonResponse({"message": "Customer does not exists"})
            response.status_code = 400
            return response
    elif request.method == "PUT":
        try:
            content = json.loads(request.body)
            customer = Customer.objects.get(id=id)

            props = ["address", "name", "phone_number"]
            for prop in props:
                if prop in content:
                    setattr(customer, prop, content[prop])
            customer.save()
            return JsonResponse(
                customer,
                encoder=CustomerEncoder,
                safe=False
            )
        except Customer.DoesNotExist:
            response = JsonResponse({"message": "Customer does not exists"})
            response.status_code = 400
            return response

    else:
        count, _ = Customer.objects.filter(id=id).delete()
        return JsonResponse({"deleted":count>0})
