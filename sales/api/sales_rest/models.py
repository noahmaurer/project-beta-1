from django.db import models



class AutomobileVO(models.Model):

    color = models.CharField(max_length = 50)
    year = models.PositiveSmallIntegerField()
    vin = models.CharField(max_length = 17, unique = True)
    import_href = models.CharField(max_length = 200, unique = True, default = "")
    status = models.CharField(max_length = 10, default =" In Stock")
    model = models.CharField(max_length = 50, default = "")

    def sold(self):
        status = "Sold"
        self.status = status
        self.save()

    def __str__(self):
        return self.model + " " + self.vin

    def __getitem__(self, key):
        return self.__dict__[key]



class SalesPerson(models.Model):
    employee_number = models.PositiveSmallIntegerField(unique = True)
    name = models.CharField(max_length = 100)

    def __str__(self):
        return self.name

    def __getitem__(self, key):
        return self.__dict__[key]


class Customer(models.Model):
    address = models.CharField(max_length = 100)
    name = models.CharField(max_length = 100)
    phone_number = models.CharField(max_length = 15)

    def __str__(self):
        return self.name


class SaleRecord(models.Model):
    automobile = models.ForeignKey(
        AutomobileVO,
        related_name = "sale_record",
        on_delete = models.PROTECT,
    )
    sales_person = models.ForeignKey(
        SalesPerson,
        related_name = "sale_record",
        on_delete = models.PROTECT
    )
    customer = models.ForeignKey(
        Customer,
        related_name = "sale_record",
        on_delete = models.PROTECT
    )
    sale_price = models.DecimalField(max_digits = 20, decimal_places = 2)

    def __str__(self):
        return self.automobile.vin

    def __getitem__(self, key):
        return self.__dict__[key]
