from django.db import models

# Create your models here.

class AutomobileVO(models.Model):
    # import_href = models.CharField(max_length=200, unique=True, null=True)
    color = models.CharField(max_length=50)
    year = models.PositiveSmallIntegerField()
    vin = models.CharField(max_length=17, unique=True)

    def __str__(self):
        return f"{self.vin}"


class Technician(models.Model):
    tech_name = models.CharField(max_length=200)
    employee_number = models.CharField(max_length=200)

    def __str__(self):
        return f"{self.tech_name}"


# two foreignkeys because they both are grabbing from the above models automobile
# is grabbing the vin/year/color from automobileVO and technician is grabbing all
# info from technician model
class ServiceAppointment(models.Model):
    automobile = models.ForeignKey(
        AutomobileVO,
        related_name="service_appointments",
        on_delete=models.PROTECT,
    )
    owner= models.CharField(max_length=200)
    appointment_datetime = models.DateTimeField()
    technician = models.ForeignKey(
        Technician,
        related_name="service_appointments",
        on_delete=models.PROTECT,
    )
    reason = models.CharField(max_length=200)
    status = models.CharField(max_length=25,default="scheduled")


    def __str__(self):
        return f"{self.owner}"
