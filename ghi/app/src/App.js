import { BrowserRouter, Routes, Route } from "react-router-dom";
import MainPage from "./MainPage";
import Nav from "./Nav";
import AutomobileList from "./Inventory/AutomobileList";
import AutomobileForm from "./Inventory/AutoMobileForm";
import ModelList from "./Inventory/ModelList";
import ModelForm from "./Inventory/ModelForm";
import ManufacturerList from "./Inventory/ManufacturerList";
import ManufacturerForm from "./Inventory/ManufacturerForm";
import CustomerForm from "./Sales/CustomerForm";
import SalesPersonForm from "./Sales/SalesPersonForm";
import SaleRecordList from "./Sales/SaleRecordList";
import SaleRecordForm from "./Sales/SaleRecordForm";
import SaleRecordsByEmployee from "./Sales/SaleRecordsByEmployee";
import TechnicianForm from "./Services/TechnicianForm";
import ServiceListAppointments from "./Services/ServiceListAppointments";
import ServiceHistory from "./Services/ServiceHistory";

function App() {
    return (
        <BrowserRouter>
            <Nav />
            <div className="container">
                <Routes>
                    <Route path="/" element={<MainPage />} />
                    <Route
                        path="/automobiles"
                        element={<AutomobileList />}></Route>
                    <Route
                        path="/automobiles/add"
                        element={<AutomobileForm />}></Route>
                    <Route path="/models" element={<ModelList />}></Route>
                    <Route path="/models/add" element={<ModelForm />}></Route>
                    <Route
                        path="/manufacturers"
                        element={<ManufacturerList />}></Route>
                    <Route
                        path="/manufacturers/add"
                        element={<ManufacturerForm />}></Route>
                    <Route
                        path="/customers/add"
                        element={<CustomerForm />}></Route>
                    <Route
                        path="/sales-persons/add"
                        element={<SalesPersonForm />}></Route>
                    <Route
                        path="/sale-records"
                        element={<SaleRecordList />}></Route>
                    <Route
                        path="/sale-records/add"
                        element={<SaleRecordForm />}></Route>
                    <Route
                        path="/sale-records/sales-person/search"
                        element={<SaleRecordsByEmployee />}></Route>
                    <Route
                        path="/technicians/add"
                        element={<TechnicianForm />}></Route>
                    <Route
                        path="/appointments"
                        element={<ServiceListAppointments />}></Route>
                    <Route
                        path="/appointments/history"
                        element={<ServiceHistory />}></Route>
                </Routes>
            </div>
        </BrowserRouter>
    );
}

export default App;
