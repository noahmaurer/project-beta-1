import React, { useState, useEffect, } from "react";

const ServiceAppointmentForm = () => {
    const initialState ={
    vin:"",
    automobiles:[],
    owner:"",
    appointment_datetime:"",
    technician:"",
    all_techs:[],
    reason:"",
    };


    const [vin, setvin] = useState(initialState.vin);
    const [owner, setowner] = useState(initialState.owner);
    const [technician, settechnician] = useState(initialState.technician);
    const [all_techs, setall_techs] = useState(
        initialState.all_techs
    );
    const [reason, setreason] = useState(initialState.reason);
    const [appointment_datetime, setdatetime] = useState(initialState.appointment_datetime);
    const [automobiles,setAutomobile] = useState(initialState.automobiles)
    const gettechnician = () => {
        fetch("http://localhost:8080/api/technicians/")
            .then((response) => {
                if (response.ok) {
                    return response.json();
                }
                throw new Error("service appointment form server error");
            })
            .then((response) =>
                setall_techs(
                    response.technicians
                )
            )
            .catch((err) => console.log(err));
    };
    useEffect(() => {
        gettechnician();
        fetch("http://localhost:8100/api/automobiles/")
            .then((resposneAuto) => {
                if (resposneAuto.ok) {
                    return resposneAuto.json();
                }
                throw new Error("Automobiles server error");
            })
            .then((resposneAuto) =>
                setAutomobile(
                    resposneAuto.autos
                )
            )
            .catch((err) => console.log(err));
    }, []);

    async function onSubmit(event) {
        event.preventDefault();
        const data = {
            vin:vin,
            owner:owner,
            appointment_datetime:appointment_datetime,
            technician:technician,
            reason:reason,
        };

        const serviceAppointmentUrl = "http://localhost:8080/api/services/";
        const fetchConfig = {
            method: "post",
            body: JSON.stringify(data),
            headers: {
                "Content-Type": "applications/json",
            },
        };
        await fetch(serviceAppointmentUrl, fetchConfig);

        const clearState = () => {
            setvin(initialState.vin);
            setowner(initialState.owner);
            // setall_techs(initialState.all_techs);
            setdatetime(initialState.appointment_datetime);
            settechnician(initialState.technician);
            setreason(initialState.reason);
        };
        clearState();
        gettechnician();

        return (
            <div className="my-5 container">
                <div className="offset-3 col-6">
                    <div className="shadow p-4 mt-4">
                        <h1>Add a Service Appointment</h1>
                        <form onSubmit={onSubmit} id="create-Service Appointment-form">
                            <div className="mb-3">
                            <div className="form-floating mb-3">
                            <input
                                placeholder="owner"
                                required
                                type="text"
                                className="form-control"
                                value={owner}
                                name="owner"
                                onChange={(event) => {
                                    setowner(event.target.value);
                                }}
                            />
                            <label htmlFor="name">Owner</label>
                        </div>
                            </div>
                            <div className="mb-3">
                                <select
                                    multiple={false}
                                    placeholder="vin"
                                    required
                                    type="text"
                                    className="form-control"
                                    value={vin}
                                    name="vin"
                                    onChange={(event) => {
                                        setvin(event.target.value);
                                    }}>
                                    <option>Choose a vehicle</option>
                                    {Array.from(automobiles).map(
                                        (auto) => {
                                            return (
                                                <option
                                                    key={auto.id}
                                                    value={auto.id}>
                                                    {auto.vin}
                                                </option>
                                            );
                                        }
                                    )}
                                </select>
                            </div>
                            <div className="form-floating mb-3">
                            <input
                                placeholder="dateTime"
                                required
                                type="text"
                                className="form-control"
                                value={appointment_datetime}
                                name="datetime"
                                onChange={(event) => {
                                    setdatetime(event.target.value);
                                }}
                            />
                            <label htmlFor="name">Date</label>
                        </div>
                            <div className="form-floating mb-3">
                            <input
                                placeholder="reason"
                                required
                                type="text"
                                className="form-control"
                                value={reason}
                                name="reason"
                                onChange={(event) => {
                                    setreason(event.target.value);
                                }}
                            />
                            <label htmlFor="name">Reason</label>
                        </div>
                            <div className="mb-3">
                                <select
                                    multiple={false}
                                    placeholder="technician"
                                    required
                                    type="text"
                                    className="form-control"
                                    value={technician}
                                    name="technician"
                                    onChange={(event) => {
                                        settechnician(event.target.value);
                                    }}>
                                    <option>Choose a technician</option>
                                    {Array.from(all_techs).map((tech) => {
                                        return (
                                            <option
                                                key={tech.id}
                                                value={tech.id}>
                                                {tech.name}
                                            </option>
                                        );
                                    })}
                                </select>
                            </div>
                            <button className="btn btn-primary">Create</button>
                        </form>
                    </div>
                </div>
            </div>
        );
    };
};
export default ServiceAppointmentForm;
