import { NavLink } from "react-router-dom";

function ServiceDropdown() {
    return (
        <li className="nav-item dropdown">
            <p
                style={{ paddingTop: "23px" }}
                className="nav-link dropdown-toggle"
                id="navbarDarkDropdownMenuLink"
                role="button"
                data-bs-toggle="dropdown"
                aria-expanded="false">
                Services
            </p>

            <ul
                className="dropdown-menu"
                aria-labelledby="navbarDropdownMenuLink">
                <li>
                    <NavLink
                        className="dropdown-item "
                        aria-current="page"
                        to="/technicians/add">
                        Add Technican
                    </NavLink>
                </li>
                <li>
                    <NavLink
                        className="dropdown-item "
                        aria-current="page"
                        to="/appointments/">
                        Service Appointments
                    </NavLink>
                </li>
                <li>
                    <NavLink
                        className="dropdown-item "
                        aria-current="page"
                        to="/appointments/history/">
                        Service History
                    </NavLink>
                </li>
            </ul>
        </li>
    );
}

export default ServiceDropdown;
