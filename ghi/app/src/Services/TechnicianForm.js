import React, { useState } from "react";

const TechnicianForm = () => {
    const initialState = {
        tech_name: "",
        employee_number: "",
    };

    const [tech_name, setName] = useState(initialState.tech_name);
    const [employee_number, setEmployeeNumber] = useState(
        initialState.employee_number
    );
    const onSubmit = (event) => {
        event.preventDefault();
        const data = {
            employee_number: employee_number,
            tech_name: tech_name,
        };

        const employeeUrl = "http://localhost:8080/api/technicians/";
        const fetchConfig = {
            method: "post",
            mode: "no-cors",
            body: JSON.stringify(data),
            headers: {
                "Content-Type": "applications/json",
            },
        };
        const response = fetch(employeeUrl, fetchConfig);
        if (response.ok) {
            const newtechnician = response.json();
        }

        const clearState = () => {
            setName(initialState.tech_name);
            setEmployeeNumber(initialState.employee_number);
        };
        clearState();
    };
    return (
        <div className="my-5 container">
            <div className="offset-3 col-6">
                <div className="shadow p-4 mt-4">
                    <h1>Create a Technican!</h1>
                    <form onSubmit={onSubmit} id="create-manufacturer-form">
                        <div className="form-floating mb-3">
                            <input
                                placeholder="Name"
                                required
                                type="text"
                                className="form-control"
                                value={tech_name}
                                name="name"
                                onChange={(event) => {
                                    setName(event.target.value);
                                }}
                            />
                            <label htmlFor="name">Name</label>
                        </div>
                        <div className="form-floating mb-3">
                            <input
                                placeholder="Employee Number"
                                required
                                type="text"
                                className="form-control"
                                value={employee_number}
                                name="name"
                                onChange={(event) => {
                                    setEmployeeNumber(event.target.value);
                                }}
                            />
                            <label htmlFor="name">Employee Number</label>
                        </div>
                        <button className="btn btn-primary">Create</button>
                    </form>
                </div>
            </div>
        </div>
    );
};

export default TechnicianForm;
