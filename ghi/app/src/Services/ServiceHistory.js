import { useEffect, useState } from "react";

function ServiceHistory() {
    const initialState = {
        vin: "",
        ServiceAppointment: "",
    };
    const [vin, setvin] = useState(initialState.vin);
    const [ServiceAppointment, setServiceAppointment] = useState(
        initialState.ServiceAppointment
    );
    useEffect(() => {
        fetch("http://localhost:8080/api/services/")
            .then((response) => {
                if (response.ok) {
                    return response.json();
                }
                throw new Error("Service History server error");
            })
            .then((response) =>
                setServiceAppointment(response.service_appointments)
            )
            .catch((err) => console.log(err));
    }, []);
    return (
        <div className="container">
            <div className="row">
                <div id="alert">
                    <div></div>
                </div>
                <div className="offset-3 col-6">
                    <div className="shadow p-4 mt-4">
                        <h1>Service Appointments </h1>
                        <form onSubmit={setServiceAppointment}>
                            <div className="mb-3">
                                <input
                                    value={vin}
                                    placeholder="vin"
                                    required
                                    type="text"
                                    className=""
                                    name="vin"
                                    onChange={(event) => {
                                        setvin(event.target.value);
                                    }}></input>
                            </div>
                            <div></div>
                        </form>
                        <div>
                            <table className="table table-striped">
                                <thead>
                                    <tr>
                                        <th>VIN</th>
                                        <th>Customer name</th>
                                        <th>Date</th>
                                        <th>Technician</th>
                                        <th>Reason</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    {Array.from(ServiceAppointment)
                                        .filter(
                                            (x) => x.automobile["vin"] == vin
                                        )
                                        .map((serviceAppointment) => {
                                            return (
                                                <tr key={serviceAppointment.id}>
                                                    <td>
                                                        {
                                                            serviceAppointment
                                                                .automobile[
                                                                "vin"
                                                            ]
                                                        }
                                                    </td>
                                                    <td>
                                                        {
                                                            serviceAppointment.owner
                                                        }
                                                    </td>
                                                    <td>
                                                        {
                                                            serviceAppointment.appointment_date
                                                        }
                                                    </td>
                                                    <td>
                                                        {
                                                            serviceAppointment
                                                                .technician[
                                                                "tech_name"
                                                            ]
                                                        }
                                                    </td>
                                                    <td>
                                                        {
                                                            serviceAppointment.reason
                                                        }
                                                    </td>
                                                </tr>
                                            );
                                        })}
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    );
}

export default ServiceHistory;
