import React, { useState, useEffect } from "react";

function ServiceListAppointments() {
    const [serviceAppointments, setServiceAppointments] = useState(null);
    useEffect(() => {
        fetch("http://localhost:8080/api/services/")
            .then((response) => {
                if (response.ok) {
                    return response.json();
                }
                throw new Error("Service Appointment server bad response");
            })
            .then((response) =>
                setServiceAppointments(response.service_appointments)
            )
            .catch((err) => console.log(err));
    }, []);

    return (
        <div className="my-5 container">
            <h1>Service List Appointments</h1>
            <table className="table table-striped w-75 mx-auto">
                <thead>
                    <tr>
                        <th>VIN</th>
                        <th>Date and Time</th>
                        <th>Customer Name</th>
                        <th>Assigned Technican</th>
                        <th>Reason of Service Appointment</th>
                    </tr>
                </thead>
                <tbody>
                    {serviceAppointments &&
                        serviceAppointments.map((service_appointment) => {
                            return (
                                <tr key={service_appointment.id}>
                                    <td className="col-sm-2">
                                        {service_appointment.automobile["vin"]}
                                    </td>
                                    <td className="col-sm-3">
                                        {
                                            service_appointment.appointment_datetime
                                        }
                                    </td>
                                    <td className="col-sm-2">
                                        {service_appointment.owner}
                                    </td>
                                    <td className="col-sm-3">
                                        {
                                            service_appointment.technician[
                                                "tech_name"
                                            ]
                                        }
                                    </td>
                                    <td className="col-sm-1">
                                        {service_appointment.reason}
                                    </td>
                                </tr>
                            );
                        })}
                </tbody>
            </table>
        </div>
    );
}

export default ServiceListAppointments;
