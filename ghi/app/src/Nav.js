import { NavLink } from "react-router-dom";
import InventoryDropDown from "./Inventory/InventoryDropdown";
import SalesDropdown from "./Sales/SalesDropdown";
import ServiceDropdown from "./Services/ServiceDropdown";

function Nav() {
    return (
        <nav
            className="navbar navbar-expand-md navbar-dark"
            style={{ backgroundColor: "#497FBD" }}>
            <div className="container-fluid">
                <NavLink className="navbar-brand fs-2 fst-italic" to={"/"}>
                    CarCar
                </NavLink>
                <button
                    className="navbar-toggler"
                    type="button"
                    data-bs-toggle="collapse"
                    data-bs-target="#navbarNavDropdown"
                    aria-controls="navbarNavDropdown"
                    aria-expanded="false"
                    aria-label="Toggle navigation">
                    <span className="navbar-toggler-icon"></span>
                </button>
                <div
                    className="collapse navbar-collapse"
                    id="navbarNavDropdown">
                    <ul className="navbar-nav">
                        <InventoryDropDown />
                        <SalesDropdown />
                        <ServiceDropdown />
                    </ul>
                </div>
            </div>
        </nav>
    );
}

export default Nav;
