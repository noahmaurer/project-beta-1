import { NavLink } from "react-router-dom";

function InventoryDropDown() {
    return (
        <li className="nav-item dropdown">
            <p
                style={{ paddingTop: "23px" }}
                className="nav-link dropdown-toggle"
                id="navbarDarkDropdownMenuLink"
                role="button"
                data-bs-toggle="dropdown"
                aria-expanded="false">
                Inventory
            </p>

            <ul
                className="dropdown-menu"
                aria-labelledby="navbarDropdownMenuLink">
                <li>
                    <NavLink className="dropdown-item" to="/automobiles/">
                        Automobiles
                    </NavLink>
                </li>
                <li>
                    <NavLink className="dropdown-item" to="/automobiles/add/">
                        Add Automobile
                    </NavLink>
                </li>
                <li>
                    <NavLink className="dropdown-item" to="/models/">
                        Vehcile Models
                    </NavLink>
                </li>
                <li>
                    <NavLink className="dropdown-item" to="/models/add/">
                        Add Vehcile Models
                    </NavLink>
                </li>
                <li>
                    <NavLink className="dropdown-item" to="/manufacturers/">
                        Manufacturers
                    </NavLink>
                </li>
                <li>
                    <NavLink className="dropdown-item" to="/manufacturers/add/">
                        Add Manufacturer
                    </NavLink>
                </li>
            </ul>
        </li>
    );
}
export default InventoryDropDown;
