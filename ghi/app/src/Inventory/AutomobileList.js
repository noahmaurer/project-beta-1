import { useEffect, useState } from "react";

function AutomobileList() {
    const [autos, setAutos] = useState(null);
    useEffect(() => {
        fetch("http://localhost:8100/api/automobiles/")
            .then((response) => {
                if (response.ok) {
                    return response.json();
                }
                throw new Error("Automobile server bad resonse");
            })
            .then((response) => setAutos(response.autos))
            .catch((err) => console.log(err));
    }, []);
    return (
        <div className="my-5 container">
            <h1>Automobiles</h1>
            <table className="table table-striped w-75 mx-auto">
                <thead>
                    <tr>
                        <th>Vin</th>
                        <th>Color</th>
                        <th>Year</th>
                        <th>Model</th>
                        <th>Manufacturer</th>
                    </tr>
                </thead>
                <tbody>
                    {autos &&
                        autos.map((auto) => {
                            return (
                                <tr key={auto.id}>
                                    <td className="col-sm-3">{auto.vin}</td>
                                    <td className="col-sm-1">{auto.color}</td>
                                    <td className="col-sm-1">{auto.year}</td>
                                    <td className="col-sm-1">
                                        {auto.model["name"]}
                                    </td>
                                    <td className="col-sm-1">
                                        {auto.model["manufacturer"]["name"]}
                                    </td>
                                </tr>
                            );
                        })}
                </tbody>
            </table>
        </div>
    );
}

export default AutomobileList;
