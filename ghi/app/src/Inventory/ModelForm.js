import React, { useState, useEffect } from "react";

const ModelForm = () => {
    const initialState = {
        name: "",
        picture_url: "",
        manufacturer_id: "",
        manufacturers: "",
    };

    const [name, setName] = useState(initialState.name);
    const [picture_url, setPictureUrl] = useState(initialState.picture_url);
    const [manufacturers, setManufacturers] = useState(
        initialState.manufacturer_id
    );
    const [manufacturer_id, setManufacturerId] = useState(
        initialState.manufacturers
    );

    useEffect(() => {
        fetch("http://localhost:8100/api/manufacturers/")
            .then((response) => {
                if (response.ok) {
                    return response.json();
                }
                throw new Error("Server says bad response");
            })
            .then((response) => setManufacturers(response.manufacturers))
            .catch((err) => console.log(err));
    }, []);

    const onSubmit = (event) => {
        event.preventDefault();
        const data = {
            name: name,
            picture_url: picture_url,
            manufacturer_id: manufacturer_id,
        };

        const vehicleUrl = "http://localhost:8100/api/models/";
        const fetchConfig = {
            method: "post",
            body: JSON.stringify(data),
            headers: {
                "Content-Type": "applications/json",
            },
        };
        fetch(vehicleUrl, fetchConfig);

        const clearState = () => {
            setName(initialState.name);
            setPictureUrl(initialState.picture_url);
            setManufacturerId(initialState.manufacturer_id);
            setManufacturers(initialState.manufacturers);
        };
        clearState();
    };
    return (
        <div className="my-5 container">
            <div className="offset-3 col-6">
                <div className="shadow p-4 mt-4">
                    <h1>Add a Vehicle Model</h1>
                    <form onSubmit={onSubmit} id="create-vehicle-model-form">
                        <div className="form-floating mb-3">
                            <input
                                placeholder="Name"
                                required
                                type="text"
                                className="form-control"
                                value={name}
                                name="name"
                                onChange={(event) => {
                                    setName(event.target.value);
                                }}
                            />
                            <label htmlFor="name">Name</label>
                        </div>
                        <div className="form-floating mb-3">
                            <input
                                placeholder="Picture URL"
                                required
                                type="text"
                                className="form-control"
                                value={picture_url}
                                name="picture_url"
                                onChange={(event) => {
                                    setPictureUrl(event.target.value);
                                }}
                            />
                            <label htmlFor="name">Picture URL</label>
                        </div>
                        <div className="mb-3">
                            <select
                                multiple={false}
                                placeholder="Manufacturer"
                                required
                                type="number"
                                className="form-control"
                                value={manufacturer_id}
                                name="manufacturer"
                                onChange={(event) => {
                                    setManufacturerId(event.target.value);
                                }}>
                                <option>Choose a Manufacturer</option>
                                {Array.from(manufacturers).map(
                                    (manufacturer) => {
                                        return (
                                            <option
                                                key={manufacturer.id}
                                                value={manufacturer.id}>
                                                {" "}
                                                {manufacturer.name}
                                            </option>
                                        );
                                    }
                                )}
                            </select>
                        </div>
                        <button className="btn btn-primary">Create</button>
                    </form>
                </div>
            </div>
        </div>
    );
};

export default ModelForm;
