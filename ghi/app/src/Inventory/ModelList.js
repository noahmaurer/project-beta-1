import { useEffect, useState } from "react";

function ModelList() {
    const [models, setModels] = useState(null);
    useEffect(() => {
        fetch("http://localhost:8100/api/models/")
            .then((res) => {
                if (res.ok) {
                    return res.json();
                }
                throw new Error("Server says bad resonse");
            })
            .then((res) => setModels(res.models))
            .catch((err) => console.log(err));
    }, []);
    return (
        <div className="my-5 container">
            <h1>Vehicle Models</h1>
            <table className="table table-striped w-75 mx-auto">
                <thead>
                    <tr>
                        <th>Name</th>
                        <th>Manufacturer</th>
                        <th>Picture</th>
                    </tr>
                </thead>
                <tbody>
                    {models &&
                        models.map((model) => {
                            return (
                                <tr key={model.id}>
                                    <td className="col-sm-2">{model.name}</td>
                                    <td className="col-sm-2">
                                        {model.manufacturer["name"]}
                                    </td>

                                    <td className="col-sm-3">
                                        <img
                                            alt="Car"
                                            className="img-thumbnail p-3"
                                            src={model["picture_url"]}
                                        />
                                    </td>
                                </tr>
                            );
                        })}
                </tbody>
            </table>
        </div>
    );
}

export default ModelList;
