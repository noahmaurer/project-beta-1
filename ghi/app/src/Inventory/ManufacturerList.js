import { useEffect, useState } from "react";

function ManufacturerList() {
    const [manufacturers, setManufacturer] = useState(null);
    useEffect(() => {
        fetch("http://localhost:8100/api/manufacturers/")
            .then((res) => {
                if (res.ok) {
                    return res.json();
                }
                throw new Error("Server says bad resonse");
            })
            .then((res) => setManufacturer(res.manufacturers))
            .catch((err) => console.log(err));
    }, []);
    return (
        <div className="my-5 container">
            <h1>Manufacturers</h1>
            <table className="table table-striped w-75 mx-auto">
                <thead>
                    <tr>
                        <th>Name</th>
                    </tr>
                </thead>
                <tbody>
                    {manufacturers &&
                        manufacturers.map((manufacturer) => {
                            return (
                                <tr key={manufacturer.id}>
                                    <td className="col-sm-1">
                                        {manufacturer.name}
                                    </td>
                                </tr>
                            );
                        })}
                </tbody>
            </table>
        </div>
    );
}

export default ManufacturerList;
