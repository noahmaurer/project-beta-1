import React, { useState, useEffect } from "react";

const AutomobileForm = () => {
    const initialState = {
        color: "",
        year: "",
        vin: "",
        model_id: "",
        models: [],
    };

    const [color, setColor] = useState(initialState.color);
    const [year, setYear] = useState(initialState.year);
    const [vin, setVin] = useState(initialState.vin);
    const [model_id, setModelId] = useState(initialState.model_id);
    const [models, setModels] = useState(initialState.models);

    useEffect(() => {
        fetch("http://localhost:8100/api/models/")
            .then((response) => {
                if (response.ok) {
                    return response.json();
                }
                throw new Error("Server says bad response");
            })
            .then((response) => setModels(response.models))
            .catch((err) => console.log(err));
    }, []);

    const onSubmit = (event) => {
        event.preventDefault();
        const data = {
            color: color,
            year: year,
            vin: vin,
            model_id: model_id,
        };

        const automobileUrl = "http://localhost:8100/api/automobiles/";
        const fetchConfig = {
            method: "post",
            body: JSON.stringify(data),
            headers: {
                "Content-Type": "applications/json",
            },
        };
        fetch(automobileUrl, fetchConfig);

        const clearState = () => {
            setColor(initialState.color);
            setYear(initialState.year);
            setVin(initialState.vin);
            setModelId(initialState.model_id);
            // setModels(initialState.models);
        };
        clearState();
    };

    return (
        <div className="my-5 container">
            <div className="offset-3 col-6">
                <div className="shadow p-4 mt-4">
                    <h1>Add an Automobile</h1>
                    <form onSubmit={onSubmit} id="create-vehicle-model-form">
                        <div className="form-floating mb-3">
                            <input
                                placeholder="Color"
                                required
                                type="text"
                                className="form-control"
                                value={color}
                                name="color"
                                onChange={(event) => {
                                    setColor(event.target.value);
                                }}
                            />
                            <label htmlFor="name">Color</label>
                        </div>
                        <div className="form-floating mb-3">
                            <input
                                placeholder="year"
                                required
                                type="text"
                                className="form-control"
                                value={year}
                                name="year"
                                onChange={(event) => {
                                    setYear(event.target.value);
                                }}
                            />
                            <label htmlFor="name">Year</label>
                        </div>
                        <div className="form-floating mb-3">
                            <input
                                placeholder="vin"
                                required
                                type="text"
                                className="form-control"
                                value={vin}
                                name="vin"
                                onChange={(event) => {
                                    setVin(event.target.value);
                                }}
                            />
                            <label htmlFor="name">Vin</label>
                        </div>
                        <div className="mb-3">
                            <select
                                placeholder="Vehicle Model"
                                required
                                type="text"
                                className="form-control"
                                value={model_id}
                                name="vehicle_model"
                                onChange={(event) => {
                                    setModelId(event.target.value);
                                }}>
                                <option>Choose a Vehicle Model</option>
                                {Array.from(models).map((model) => {
                                    return (
                                        <option key={model.id} value={model.id}>
                                            {`${model.manufacturer["name"]} - ${model.name}`}
                                        </option>
                                    );
                                })}
                            </select>
                        </div>
                        <button className="btn btn-primary">Create</button>
                    </form>
                </div>
            </div>
        </div>
    );
};
export default AutomobileForm;
