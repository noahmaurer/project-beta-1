import React, { useState } from "react";

const ManufacturerForm = () => {
    const initialState = "";

    const [name, setName] = useState(initialState);

    const onSubmit = (event) => {
        event.preventDefault();
        const data = { name: name };
        console.log(data);

        const manufacturerUrl = "http://localhost:8100/api/manufacturers/";
        const fetchConfig = {
            method: "post",
            body: JSON.stringify(data),
            headers: {
                "Content-Type": "applications/json",
            },
        };
        const response = fetch(manufacturerUrl, fetchConfig);
        if (response.ok) {
            const newManufacturer = response.json();
            console.log(newManufacturer);
        }

        const clearState = () => {
            setName(initialState);
        };
        clearState();
    };

    return (
        <div className="my-5 container">
            <div className="offset-3 col-6">
                <div className="shadow p-4 mt-4">
                    <h1>Add a manufacturer</h1>
                    <form onSubmit={onSubmit} id="create-manufacturer-form">
                        <div className="form-floating mb-3">
                            <input
                                placeholder="Name"
                                required
                                type="text"
                                className="form-control"
                                value={name}
                                name="name"
                                onChange={(event) => {
                                    setName(event.target.value);
                                }}
                            />
                            <label htmlFor="name">Name</label>
                        </div>
                        <button className="btn btn-primary">Create</button>
                    </form>
                </div>
            </div>
        </div>
    );
};

export default ManufacturerForm;
