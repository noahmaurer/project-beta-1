import { NavLink } from "react-router-dom";

function SalesDropdown() {
    return (
        <li className="nav-item dropdown">
            <p
                style={{ paddingTop: "23px" }}
                className="nav-link dropdown-toggle"
                id="navbarDarkDropdownMenuLink"
                role="button"
                data-bs-toggle="dropdown"
                aria-expanded="false">
                Sales
            </p>

            <ul
                className="dropdown-menu"
                aria-labelledby="navbarDropdownMenuLink">
                <li>
                    <NavLink
                        className="dropdown-item "
                        aria-current="page"
                        to="/customers/add/">
                        Add Customer
                    </NavLink>
                </li>
                <li>
                    <NavLink
                        className="dropdown-item "
                        aria-current="page"
                        to="/sales-persons/add/">
                        Add Sales Person
                    </NavLink>
                </li>
                <li>
                    <NavLink className="dropdown-item " to="/sale-records/">
                        Sale Records
                    </NavLink>
                </li>
                <li>
                    <NavLink
                        className="dropdown-item "
                        aria-current="page"
                        to="/sale-records/add/">
                        Add Sale Record
                    </NavLink>
                </li>
                <li>
                    <NavLink
                        className="dropdown-item "
                        // aria-current="true"
                        to="/sale-records/sales-person/search/">
                        Sale Records by Sales Person
                    </NavLink>
                </li>
            </ul>
        </li>
    );
}

export default SalesDropdown;
