import { useEffect, useState } from "react";

function SaleRecordsByEmployee() {
    const initialState = {
        id: "",
        sales_persons: [],
        sale_records: [],
    };

    const [id, setId] = useState(initialState.id);
    const [sales_persons, setSalesPersons] = useState(
        initialState.sales_persons
    );
    const [sale_records, setSaleRecords] = useState(initialState.sale_records);

    useEffect(() => {
        fetch("http://localhost:8090/api/sales_persons/")
            .then((response) => {
                if (response.ok) {
                    return response.json();
                }
                throw new Error("Sale Records server error");
            })
            .then((response) => setSalesPersons(response.sales_persons))
            .catch((err) => console.log(err));
    }, []);

    const getSalesPersonRecords = () => {
        fetch(`http://localhost:8090/api/sale_records/`)
            .then((response) => {
                if (response.ok) {
                    return response.json();
                }
                throw new Error("Sale Records server error");
            })
            .then((response) => setSaleRecords(response.sale_records))
            .catch((err) => console.log(err));
    };

    return (
        <div className="my-5 container">
            <div className="shadow p-4 mt-4">
                <h1>Sales Person History</h1>
                <select
                    multiple={false}
                    placeholder="Sales Persons"
                    required
                    type="text"
                    className="form-select form-select-lg mb-3"
                    name="automobile"
                    onChange={(event) => {
                        setId(event.target.value);
                        getSalesPersonRecords();
                    }}>
                    <option>Choose an Sales Persons</option>
                    {Array.from(sales_persons).map((sale_person) => {
                        return (
                            <option key={sale_person.id} value={sale_person.id}>
                                {sale_person.name}
                            </option>
                        );
                    })}
                </select>
                <table className="table table-striped mx-auto">
                    <thead>
                        <tr>
                            <th>Sales Person</th>
                            <th>Customer</th>
                            <th>VIN</th>
                            <th>Sale Price</th>
                        </tr>
                    </thead>
                    <tbody>
                        {sale_records &&
                            sale_records
                                .filter((auto) => auto.sales_person["id"] == id)
                                .map((sale_record) => {
                                    return (
                                        <tr key={sale_record.id}>
                                            <td className="col-sm-2">
                                                {
                                                    sale_record.sales_person[
                                                        "name"
                                                    ]
                                                }
                                            </td>
                                            <td className="col-sm-3">
                                                {sale_record.customer["name"]}
                                            </td>
                                            <td className="col-sm-2">
                                                {sale_record.automobile["vin"]}
                                            </td>

                                            <td className="col-sm-3">
                                                ${sale_record.sale_price}
                                            </td>
                                        </tr>
                                    );
                                })}
                    </tbody>
                </table>
            </div>
        </div>
    );
}

export default SaleRecordsByEmployee;
