import React, { useState } from "react";

const CustomerForm = () => {
    const initialState = {
        address: "",
        name: "",
        phone_number: "",
    };

    const [name, setName] = useState(initialState.name);
    const [address, setAddress] = useState(initialState.address);
    const [phone_number, setPhoneNumber] = useState(initialState.phone_number);

    const onSubmit = (event) => {
        event.preventDefault();
        const data = {
            address: address,
            name: name,
            phone_number: phone_number,
        };

        const customerUrl = "http://localhost:8090/api/customers/";
        const fetchConfig = {
            method: "post",
            mode: "no-cors",
            body: JSON.stringify(data),
            headers: {
                "Content-Type": "applications/json",
            },
        };
        const response = fetch(customerUrl, fetchConfig);
        if (response.ok) {
            const newCustomer = response.json();
            console.log(newCustomer);
        }

        const clearState = () => {
            setName(initialState.name);
            setAddress(initialState.address);
            setPhoneNumber(initialState.phone_number);
        };
        clearState();
    };

    return (
        <div className="my-5 container">
            <div className="offset-3 col-6">
                <div className="shadow p-4 mt-4">
                    <h1>Add a Customer</h1>
                    <form onSubmit={onSubmit} id="create-manufacturer-form">
                        <div className="form-floating mb-3">
                            <input
                                placeholder="Name"
                                required
                                type="text"
                                className="form-control"
                                value={name}
                                name="name"
                                onChange={(event) => {
                                    setName(event.target.value);
                                }}
                            />
                            <label htmlFor="name">Name</label>
                        </div>
                        <div className="form-floating mb-3">
                            <input
                                placeholder="Address"
                                required
                                type="text"
                                className="form-control"
                                value={address}
                                name="address"
                                onChange={(event) => {
                                    setAddress(event.target.value);
                                }}
                            />
                            <label htmlFor="name">Address</label>
                        </div>
                        <div className="mb-3">
                            <label htmlFor="name">Phone Number</label>
                            <input
                                placeholder="(XXX)-XXX-XXXX"
                                required
                                type="text"
                                className="form-control"
                                value={phone_number}
                                name="phone_number"
                                onChange={(event) => {
                                    setPhoneNumber(event.target.value);
                                }}
                            />
                        </div>
                        <button className="btn btn-primary">Create</button>
                    </form>
                </div>
            </div>
        </div>
    );
};

export default CustomerForm;
