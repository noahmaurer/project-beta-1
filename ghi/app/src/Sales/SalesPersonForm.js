import React, { useState } from "react";

const SalesPersonForm = () => {
    const initialState = {
        employee_number: "",
        name: "",
    };

    const [name, setName] = useState(initialState.name);
    const [employee_number, setEmployeeNumber] = useState(
        initialState.employee_number
    );

    const onSubmit = (event) => {
        event.preventDefault();
        const data = {
            employee_number: employee_number,
            name: name,
        };

        const employeeUrl = "http://localhost:8090/api/sales_persons/";
        const fetchConfig = {
            method: "post",
            mode: "no-cors",
            body: JSON.stringify(data),
            headers: {
                "Content-Type": "applications/json",
            },
        };
        const response = fetch(employeeUrl, fetchConfig);
        if (response.ok) {
            const newSalesPerson = response.json();
            console.log(newSalesPerson);
        }

        const clearState = () => {
            setName(initialState.name);
            setEmployeeNumber(initialState.employee_number);
        };
        clearState();
    };

    return (
        <div className="my-5 container">
            <div className="offset-3 col-6">
                <div className="shadow p-4 mt-4">
                    <h1>Add a Sales Person</h1>
                    <form onSubmit={onSubmit} id="create-sales-person-form">
                        <div className="form-floating mb-3">
                            <input
                                placeholder="Name"
                                required
                                type="text"
                                className="form-control"
                                value={name}
                                name="name"
                                onChange={(event) => {
                                    setName(event.target.value);
                                }}
                            />
                            <label htmlFor="name">Name</label>
                        </div>
                        <div className="form-floating mb-3">
                            <input
                                placeholder="Employee Number"
                                required
                                type="text"
                                className="form-control"
                                value={employee_number}
                                name="name"
                                onChange={(event) => {
                                    setEmployeeNumber(event.target.value);
                                }}
                            />
                            <label htmlFor="name">Employee Number</label>
                        </div>
                        <button className="btn btn-primary">Create</button>
                    </form>
                </div>
            </div>
        </div>
    );
};

export default SalesPersonForm;
