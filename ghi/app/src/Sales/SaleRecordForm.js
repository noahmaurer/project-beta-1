import React, { useState, useEffect } from "react";

const SaleRecordForm = () => {
    const initialState = {
        sale_price: "",
        automobile: "",
        automobiles: [],
        sales_person: "",
        sales_persons: [],
        customer: "",
        customers: [],
    };

    const [sale_price, setSalePrice] = useState(initialState.sale_price);
    const [automobile, setAutomobile] = useState(initialState.automobile);
    const [automobiles, setAutomobiles] = useState(initialState.automobiles);
    const [sales_person, setSalesPerson] = useState(initialState.sales_person);
    const [sales_persons, setSalesPersons] = useState(
        initialState.sales_persons
    );
    const [customer, setCustomer] = useState(initialState.customer);
    const [customers, setCustomers] = useState(initialState.customers);

    const getAutomobiles = () => {
        fetch("http://localhost:8090/api/automobiles/")
            .then((responseAuto) => {
                if (responseAuto.ok) {
                    return responseAuto.json();
                }
                throw new Error("Automobiles server error");
            })
            .then((responseAuto) =>
                setAutomobiles(
                    responseAuto.autos.filter(
                        (auto) => auto["status"] === "In Stock"
                    )
                )
            )
            .catch((err) => console.log(err));
    };
    useEffect(() => {
        getAutomobiles();

        fetch("http://localhost:8090/api/sales_persons/")
            .then((responseSalesPerson) => {
                if (responseSalesPerson.ok) {
                    return responseSalesPerson.json();
                }
                throw new Error("Sales Person server error");
            })
            .then((responseSalesPerson) =>
                setSalesPersons(responseSalesPerson.sales_persons)
            )
            .catch((err) => console.log(err));

        fetch("http://localhost:8090/api/customers/")
            .then((responseCustomer) => {
                if (responseCustomer.ok) {
                    return responseCustomer.json();
                }
                throw new Error("Customer server error");
            })
            .then((responseCustomer) =>
                setCustomers(responseCustomer.customers)
            )
            .catch((err) => console.log(err));
    }, []);

    async function onSubmit(event) {
        event.preventDefault();
        const data = {
            sale_price: sale_price,
            automobile: automobile,
            sales_person: sales_person,
            customer: customer,
        };

        const saleRecordUrl = "http://localhost:8090/api/sale_records/";
        const fetchConfig = {
            method: "post",
            body: JSON.stringify(data),
            headers: {
                "Content-Type": "applications/json",
            },
        };
        await fetch(saleRecordUrl, fetchConfig);

        const clearState = () => {
            setSalePrice(initialState.sale_price);
            setAutomobile(initialState.automobile);
            setSalesPerson(initialState.sales_person);
            setCustomer(initialState.customer);
        };
        clearState();
        getAutomobiles();
    }

    return (
        <div className="my-5 container">
            <div className="offset-3 col-6">
                <div className="shadow p-4 mt-4">
                    <h1>Add a Sale Record</h1>
                    <form onSubmit={onSubmit} id="create-vehicle-model-form">
                        <div className="form-floating mb-3">
                            <input
                                placeholder="Sale Price"
                                required
                                type="text"
                                className="form-control"
                                value={sale_price}
                                name="sale_price"
                                onChange={(event) => {
                                    setSalePrice(event.target.value);
                                }}
                            />
                            <label htmlFor="name">Sale Price</label>
                        </div>

                        <div className="mb-3">
                            <select
                                multiple={false}
                                placeholder="Automobile"
                                required
                                type="text"
                                className="form-control"
                                value={automobile}
                                name="automobile"
                                onChange={(event) => {
                                    setAutomobile(event.target.value);
                                }}>
                                <option>Choose an Automobile</option>
                                {Array.from(automobiles).map((auto) => {
                                    return (
                                        <option key={auto.id} value={auto.id}>
                                            {" "}
                                            {auto.vin}
                                        </option>
                                    );
                                })}
                            </select>
                        </div>
                        <div className="mb-3">
                            <select
                                multiple={false}
                                placeholder="Sales Person"
                                required
                                type="text"
                                className="form-control"
                                value={sales_person}
                                name="sales_person"
                                onChange={(event) => {
                                    setSalesPerson(event.target.value);
                                }}>
                                <option>Choose a Sales Person</option>
                                {Array.from(sales_persons).map(
                                    (sales_person) => {
                                        return (
                                            <option
                                                key={sales_person.id}
                                                value={sales_person.id}>
                                                {" "}
                                                {sales_person.name}
                                            </option>
                                        );
                                    }
                                )}
                            </select>
                        </div>
                        <div className="mb-3">
                            <select
                                multiple={false}
                                placeholder="Customer"
                                required
                                type="text"
                                className="form-control"
                                value={customer}
                                name="sales_person"
                                onChange={(event) => {
                                    setCustomer(event.target.value);
                                }}>
                                <option>Choose a Customer</option>
                                {Array.from(customers).map((customer) => {
                                    return (
                                        <option
                                            key={customer.id}
                                            value={customer.id}>
                                            {" "}
                                            {customer.name}
                                        </option>
                                    );
                                })}
                            </select>
                        </div>
                        <button className="btn btn-primary">Create</button>
                    </form>
                </div>
            </div>
        </div>
    );
};
export default SaleRecordForm;
