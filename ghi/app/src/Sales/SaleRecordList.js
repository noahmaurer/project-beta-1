import { useEffect, useState } from "react";

function SaleRecordList() {
    const [saleRecords, setSaleRecords] = useState(null);
    useEffect(() => {
        fetch("http://localhost:8090/api/sale_records/")
            .then((response) => {
                if (response.ok) {
                    return response.json();
                }
                throw new Error("Sales Record server bad response");
            })
            .then((response) => setSaleRecords(response.sale_records))
            .catch((err) => console.log(err));
    }, []);

    return (
        <div className="my-5 container">
            <h1>Sales Records</h1>
            <table className="table table-striped w-75 mx-auto">
                <thead>
                    <tr>
                        <th>Sales Person</th>
                        <th>Sales Person Number</th>
                        <th>Customer</th>
                        <th>VIN</th>
                        <th>Sale Price</th>
                    </tr>
                </thead>
                <tbody>
                    {saleRecords &&
                        saleRecords.map((sale_record) => {
                            return (
                                <tr key={sale_record.id}>
                                    <td className="col-sm-2">
                                        {sale_record.sales_person["name"]}
                                    </td>
                                    <td className="col-sm-3">
                                        {
                                            sale_record.sales_person[
                                                "employee_number"
                                            ]
                                        }
                                    </td>
                                    <td className="col-sm-2">
                                        {sale_record.customer["name"]}
                                    </td>
                                    <td className="col-sm-3">
                                        {sale_record.automobile["vin"]}
                                    </td>
                                    <td className="col-sm-3">
                                        ${sale_record.sale_price}
                                    </td>
                                    <td className="col-sm-1"></td>
                                </tr>
                            );
                        })}
                </tbody>
            </table>
        </div>
    );
}

export default SaleRecordList;
